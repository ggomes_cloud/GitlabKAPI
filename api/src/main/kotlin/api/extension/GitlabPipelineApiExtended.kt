package api.extension

import api.GitlabJobApiImpl
import api.GitlabPipelineApi
import api.GitlabPipelineApiImpl
import api.core.GitlabApiClient
import model.Job
import model.abbreviated.PipelineDurationBreakdown
import java.time.Instant

interface GitlabPipelineApiExtended: GitlabPipelineApi {
    suspend fun getPipelineBreakdown(pipelineId: Long, filter: PipelineOutcome = PipelineOutcome.ANY): List<Job>

    suspend fun getPipelinePerformanceBreakdown(pipelineId: Long): PipelineDurationBreakdown

}

internal class GitlabPipelineApiExtendedImpl(
    private val client: GitlabApiClient
): GitlabPipelineApiImpl(client), GitlabPipelineApiExtended {

    override suspend fun getPipelineBreakdown(pipelineId: Long, filter: PipelineOutcome): List<Job> {
        val jobs = GitlabJobApiImpl(client).getPipelineJobs(pipelineId)

        return jobs.filter { job ->
            filter.string.contains(job.status)
        }
    }

    override suspend fun getPipelinePerformanceBreakdown(pipelineId: Long): PipelineDurationBreakdown {
        val pipeline = getPipelineBy(pipelineId)
        val jobs = GitlabJobApiImpl(client).getPipelineJobs(pipelineId)

        val abbreviatedJobs = jobs.map { job ->
            model.abbreviated.Job(job.name, job.duration?.toLong()?.times(1000), job.created_at.toMillis(), job.web_url)
        }

        val finishedAt = abbreviatedJobs.foldRight(0L) { job, acc ->
            acc + (job.durationMillis ?: 0)
        }
        return PipelineDurationBreakdown(
            id = pipelineId,
            createdAt = pipeline.created_at.toMillis(),
            ref = pipeline.ref,
            durationMillis = finishedAt,
            url = pipeline.web_url,
            jobs = abbreviatedJobs
        )
    }

    private fun String.toMillis(): Long = Instant.parse(this).toEpochMilli()
}

enum class PipelineOutcome(val string: String) {
    SUCCESS("success"), ERROR("failure"), ANY("created, pending, running, failed, success, canceled, skipped, waiting_for_resource, or manual")
}