package api.core

import GitlabConfig
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json
import model.GitlabApiException
import model.PageEnvelope
import model.Paging

internal class GitlabApiClient(config: GitlabConfig) {
    private val client = HttpClient(CIO) {
        install(ContentNegotiation) {
            json(Json {
                prettyPrint = true
                isLenient = true
                ignoreUnknownKeys = true
            })
        }
        defaultRequest {
            header("PRIVATE-TOKEN", config.token)
            contentType(ContentType.Application.Json)
        }
    }

    private val baseUrl = config.projectBaseUrl

    @Throws(GitlabApiException::class)
    suspend inline fun <reified T> get(path: String): T = client.get(baseUrl + path)
        .requireSuccess()
        .body() as T

    @Throws(GitlabApiException::class)
    suspend inline fun <reified T> getPaged(path: String): PageEnvelope<T> {
        val response = client
            .get(baseUrl + path)
            .requireSuccess()

        return PageEnvelope(
            response = response.body() as T,
            Paging(
                next = response.headers["x-next-page"]?.toIntOrNull(),
                previous = response.headers["x-prev-page"]?.toIntOrNull(),
                totalPages = response.headers["x-total-pages"]?.toIntOrNull(),
            )
        )
    }

    @Throws(GitlabApiException::class)
    suspend inline fun <reified T, reified Y> post(path: String, body: Y): T = client.post(baseUrl + path) {
        setBody(body)
    }.requireSuccess().body() as T

    @Throws(GitlabApiException::class)
    suspend inline fun <reified T> post(path: String): T = client.post(baseUrl + path)
        .requireSuccess()
        .body() as T

    suspend inline fun delete(path: String): HttpStatusCode {
        return client.delete(baseUrl + path).status
    }

    @Throws(GitlabApiException::class)
    suspend inline fun <reified T, reified Y> put(path: String, body: Y): T = client.put(baseUrl + path) {
        setBody(body)
    }.requireSuccess().body() as T

    companion object {
        private suspend fun HttpResponse.requireSuccess(): HttpResponse {
            if (!this.status.isSuccess())
                throw GitlabApiException(
                    "[EXCEPTION] ${this.request.method.value} ${this.request.url} wasn't successful," +
                            " response below: \n${this.body<String>()} "
                )

            return this
        }
    }
}
