package api

import api.core.GitlabApiClient
import io.ktor.client.statement.*
import io.ktor.http.*
import model.Tag
import model.request.TagCreationRequest

interface GitlabTagApi {
    suspend fun getTags(): List<Tag>

    suspend fun makeTag(tagCreationRequest: TagCreationRequest): Boolean

    suspend fun deleteTag(name: String): Boolean
}

internal class GitlabTagApiImpl(private val client: GitlabApiClient) : GitlabTagApi {

    override suspend fun getTags(): List<Tag> = client.get("repository/tags")

    override suspend fun makeTag(tagCreationRequest: TagCreationRequest): Boolean {
        val response: HttpResponse = client.post("repository/tags", tagCreationRequest)

        return response.status == HttpStatusCode.Created
    }

    override suspend fun deleteTag(name: String): Boolean = client.delete("repository/tags/$name") == HttpStatusCode.NoContent
}