package api

import api.core.GitlabApiClient
import io.ktor.util.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import model.File
import model.request.FileUpdateRequest
import model.request.FileUpdated
import java.net.URLEncoder

interface GitlabFileApi {
    suspend fun getFileWithEncodedBody(ref: String, path: String): File
    suspend fun getFile(ref: String, path: String): File

    suspend fun updateFile(ref: String, path: String, branch: String, commitMessage: String, body: String): Boolean
}

internal class GitlabFileApiImpl(private val client: GitlabApiClient): GitlabFileApi {
    private val apiPath = "repository/files/"
    override suspend fun getFile(ref: String, path: String): File {
        val result = getFileWithEncodedBody(ref, path)

        return result.copy(content = result.content.decodeBase64String())
    }

    override suspend fun getFileWithEncodedBody(ref: String, path: String): File {
        val sanitisedFilePath = path.urlEncode()
        return client.get("$apiPath$sanitisedFilePath?ref=$ref")
    }

    override suspend fun updateFile(ref: String, path: String, branch: String, commitMessage: String, body: String): Boolean {
        val request = FileUpdateRequest(branch, commitMessage, body)
        val sanitisedFilePath = path.urlEncode()

        val response: FileUpdated = client.put("$apiPath$sanitisedFilePath", request)

        return response.file_path == path && response.branch == branch
    }

    private suspend fun String.urlEncode() = withContext(Dispatchers.IO) {
        URLEncoder.encode(this@urlEncode, "UTF-8")
    }
}