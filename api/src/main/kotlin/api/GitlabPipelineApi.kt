package api

import api.core.GitlabApiClient
import model.PageEnvelope
import model.Pipeline
import model.PipelineStatus

interface GitlabPipelineApi {
    suspend fun getPipelines(page: Int = 0, pageSize: Int = 20): List<Pipeline>

    suspend fun getPipelineFor(ref: String, page: Int = 0, pageSize: Int = 20): List<Pipeline>

    suspend fun getLatestPipelineFor(ref: String): PipelineStatus

    suspend fun getAllPipelines(countLimit: Int = 100): List<Pipeline>

    suspend fun getPipelineBy(pipelineId: Long): Pipeline
}

internal open class GitlabPipelineApiImpl(private val client: GitlabApiClient) : GitlabPipelineApi {
    override suspend fun getPipelines(page: Int, pageSize: Int): List<Pipeline> {
        return client.get("pipelines?page=$page&per_page=$pageSize")
    }

    override suspend fun getPipelineFor(ref: String, page: Int, pageSize: Int): List<Pipeline> {
        return client.get("pipelines?ref=$ref&page=$page&per_page=$pageSize")
    }

    override suspend fun getLatestPipelineFor(ref: String): PipelineStatus {
        return client.get("pipelines/latest?ref=$ref")
    }

    private suspend fun getPipelinesPaged(page: Int, pageSize: Int = 20): PageEnvelope<List<Pipeline>> {
        return client.getPaged("pipelines?page=$page&per_page=$pageSize")
    }

    override suspend fun getAllPipelines(countLimit: Int): List<Pipeline> {
        val pipelines = mutableListOf<Pipeline>()
        var page = 0
        while (true) {
            val pagedResponse = getPipelinesPaged(page = page)

            pipelines.addAll(pagedResponse.response)
            if (pagedResponse.paging?.next == null || pipelines.size >= countLimit) {
                break
            }

            page = pagedResponse.paging.next
        }

        return pipelines
    }

    override suspend fun getPipelineBy(pipelineId: Long): Pipeline = client.get("pipelines/$pipelineId")

}