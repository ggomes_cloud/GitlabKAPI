package api

import api.core.GitlabApiClient
import io.ktor.http.*
import model.Branch

interface GitlabBranchApi {
    suspend fun getAllBranches(): List<Branch>

    // REGEX should be formatted like so: https://github.com/google/re2/wiki/Syntax
    suspend fun getBranches(regex: String): List<Branch>

    suspend fun createBranch(name: String, ref: String): Branch

    suspend fun deleteBranch(name: String): Boolean
}

internal class GitlabBranchApiImpl(private val client: GitlabApiClient): GitlabBranchApi {
    private val path = "repository/branches"
    override suspend fun getAllBranches(): List<Branch> {
        return client.get(path)
    }

    override suspend fun getBranches(regex: String): List<Branch> {
        return client.get("$path?regex=$regex")
    }

    override suspend fun createBranch(name: String, ref: String): Branch {
        return client.post("$path?branch=$name&ref=$ref")
    }

    override suspend fun deleteBranch(name: String): Boolean {
        return client.delete("$path/$name") == HttpStatusCode.NoContent
    }

}