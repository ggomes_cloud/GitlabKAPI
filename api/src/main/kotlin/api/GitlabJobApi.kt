package api

import api.core.GitlabApiClient
import model.Job
import model.PageEnvelope

interface GitlabJobApi {
    // Resource: https://docs.gitlab.com/ee/api/jobs.html#list-pipeline-jobs
    suspend fun getPipelineJobs(
        pipelineId: Long,
        includeRetries: Boolean = false,
        countLimit: Int = 100
    ): List<Job>

    // Resource: https://docs.gitlab.com/ee/api/jobs.html#get-a-single-job
    suspend fun getJobBy(id: String): Job

    //suspend fun getAllJobs(): List<>
}

internal class GitlabJobApiImpl(private val client: GitlabApiClient) : GitlabJobApi {
    override suspend fun getPipelineJobs(
        pipelineId: Long,
        includeRetries: Boolean,
        countLimit: Int
    ): List<Job> {
        val jobs: MutableList<Job> = mutableListOf()
        var currentPage = 0
        while (true) {
            val pagedResponse = getJobsPaged(pipelineId, currentPage, 20, includeRetries)
            jobs.addAll(pagedResponse.response)

            if (pagedResponse.paging?.next == null || jobs.size >= countLimit) {
                break
            }

            currentPage = pagedResponse.paging.next
        }

        return jobs
    }

    private suspend fun getJobsPaged(pipelineId: Long, page: Int, pageSize: Int, includeRetries: Boolean): PageEnvelope<List<Job>> {
        val path = "pipelines/$pipelineId/jobs?page=$page&per_page=$pageSize&include_retried=$includeRetries"
        return client.getPaged(path)
    }

    override suspend fun getJobBy(id: String): Job = client.get("jobs/$id")
}