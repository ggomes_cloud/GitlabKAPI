import api.*
import api.core.GitlabApiClient
import api.GitlabBranchApiImpl
import api.extension.GitlabPipelineApiExtended
import api.extension.GitlabPipelineApiExtendedImpl

class GitlabApiModule(config: GitlabConfig) {
    private val client: GitlabApiClient by lazy {
        GitlabApiClient(config)
    }

    val branchApi: GitlabBranchApi by lazy {
        GitlabBranchApiImpl(client)
    }

    val pipelineApi: GitlabPipelineApi by lazy {
        GitlabPipelineApiImpl(client)
    }

    val fileApi: GitlabFileApi by lazy {
        GitlabFileApiImpl(client)
    }

    val tagApi: GitlabTagApi by lazy {
        GitlabTagApiImpl(client)
    }

    val jobApi: GitlabJobApi by lazy {
        GitlabJobApiImpl(client)
    }

    val extendedPipelineApi: GitlabPipelineApiExtended by lazy {
        GitlabPipelineApiExtendedImpl(client)
    }
}