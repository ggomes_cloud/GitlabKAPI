data class GitlabConfig(
    val projectBaseUrl: String,
    val token: String,
) {
    init {
        // Validates some stuff
        require(projectBaseUrl.endsWith("/"))
    }
}
