package model

import kotlinx.serialization.Serializable

@Serializable
data class Release(
    val tag_name: String,
    val description: String
)