package model

import kotlinx.serialization.Serializable

@Serializable
data class Job(
    val commit: JobCommit,
    val coverage: String?,
    val archived: Boolean,
    val allow_failure: Boolean,
    val created_at: String,
    val started_at: String?,
    val finished_at: String?,
    val erased_at: String?,
    val duration: Double?,
    val queued_duration: Double?,
    val artifacts_expire_at: String?,
    val tag_list: List<String>,
    val id: Long,
    val name: String,
    val pipeline: JobPipeline,
    val ref: String,
    val artifacts: List<Artifact>,
    val runner: Runner?,
    val stage: String,
    val status: String,
    val failure_reason: String? = null,
    val tag: Boolean,
    val web_url: String,
    val project: Project,
    val user: JobUser,
)

@Serializable
data class JobCommit(
    val author_email: String,
    val author_name: String,
    val created_at: String,
    val id: String,
    val message: String,
    val short_id: String,
    val title: String
) // Much smaller than the Commit class

@Serializable
data class JobPipeline(
    val id: Long,
    val project_id: Long,
    val ref: String,
    val sha: String,
    val status: String
) // Much smaller than the Job class

@Serializable
data class Project(
    val ci_job_token_scope_enabled: Boolean
)

@Serializable
data class JobUser(
    val id: Long,
    val name: String,
    val username: String,
    val state: String,
    val avatar_url: String,
    val web_url: String,
    val created_at: String,
    val bio: String?,
    val location: String?,
    val public_email: String,
    val skype: String,
    val linkedin: String,
    val twitter: String,
    val website_url: String,
    val organization: String
)

@Serializable
data class Artifact(
    val file_type: String?,
    val size: Long?,
    val filename: String?,
    val file_format: String?
)

@Serializable
data class Runner(
    val id: Long,
    val description: String?,
    val ip_address: String?,
    val active: Boolean,
    val paused: Boolean,
    val is_shared: Boolean,
    val runner_type: String?,
    val name: String?,
    val online: Boolean,
    val status: String
)
