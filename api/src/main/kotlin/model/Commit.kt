package model

import kotlinx.serialization.Serializable

@Serializable
data class Commit(
    val id: String,
    val short_id: String,
    val title: String,
    val created_at: String,
    val parent_ids: List<String>,
    val message: String,
    val author_name: String,
    val author_email: String,
    val authored_date: String,
    val committer_name: String,
    val committer_email: String,
    val committed_date: String,
    val trailers: Map<String, String>? = null,
    val extended_trailers: Map<String, String>? = null,
    val web_url: String? = null
)