package model

import kotlinx.serialization.Serializable

@Serializable
data class File(
    val file_name: String,
    val file_path: String,
    val size: Int,
    val encoding: String,
    val content: String,
    val content_sha256: String,
    val ref: String,
    val blob_id: String,
    val commit_id: String,
    val last_commit_id: String,
    val execute_filemode: Boolean
)