package model.request

import kotlinx.serialization.Serializable

@Serializable
data class FileUpdateRequest(
    val branch: String,
    val commit_message: String,
    val content: String
)