package model.request

import kotlinx.serialization.Serializable

@Serializable
data class TagCreationRequest(
    val tag_name: String,
    val ref: String,
    val message: String,
    val release_description: String
)