package model.request

import kotlinx.serialization.Serializable

@Serializable
data class FileUpdated(
    val file_path: String,
    val branch: String
)