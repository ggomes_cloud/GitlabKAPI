package model

import kotlinx.serialization.Serializable

@Serializable
data class User(
    val id: Long,
    val username: String,
    val name: String,
    val state: String,
    val locked: Boolean,
    val avatar_url: String,
    val web_url: String
)

@Serializable
data class DetailedStatus(
    val icon: String,
    val text: String,
    val label: String,
    val group: String,
    val tooltip: String,
    val has_details: Boolean,
    val details_path: String,
    val illustration: String?,
    val favicon: String
)

@Serializable
data class PipelineStatus(
    val id: Long,
    val iid: Int,
    val project_id: Long,
    val sha: String,
    val ref: String,
    val status: String,
    val source: String,
    val created_at: String,
    val updated_at: String?,
    val web_url: String,
    val before_sha: String,
    val tag: Boolean,
    val yaml_errors: String?,
    val user: User,
    val started_at: String?,
    val finished_at: String?,
    val committed_at: String?,
    val duration: Int?,
    val queued_duration: Int?,
    val coverage: String?,
    val detailed_status: DetailedStatus,
    val name: String?
)
