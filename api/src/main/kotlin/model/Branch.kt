package model

import kotlinx.serialization.Serializable

@Serializable
data class Branch(
    val name: String,
    val merged: Boolean,
    val protected: Boolean,
    val default: Boolean,
    val developers_can_push: Boolean,
    val developers_can_merge: Boolean,
    val can_push: Boolean,
    val web_url: String,
    val commit: Commit
)
