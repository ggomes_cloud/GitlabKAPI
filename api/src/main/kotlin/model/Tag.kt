package model

import kotlinx.serialization.Serializable

@Serializable
data class Tag(
    val commit: Commit,
    val release: Release?,
    val name: String,
    val target: String,
    val message: String?,
    val protected: Boolean
)
