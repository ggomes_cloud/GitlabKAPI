package model

class GitlabApiException(
    message: String,
    throwable: Throwable? = null
): RuntimeException(message, throwable)