package model

data class PageEnvelope<T>(
    val response: T,
    val paging: Paging? = null,
)

data class Paging(
    val next: Int?,
    val previous: Int?,
    val totalPages: Int?,
)
