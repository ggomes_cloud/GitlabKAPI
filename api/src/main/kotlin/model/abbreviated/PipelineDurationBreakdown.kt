package model.abbreviated

data class PipelineDurationBreakdown(
    val id: Long,
    val createdAt: Long,
    val ref: String,
    val durationMillis: Long,
    val url: String,
    val jobs: List<Job>,
)

data class Job(
    val name: String,
    val durationMillis: Long?,
    val createdAt: Long,
    val url: String
)
