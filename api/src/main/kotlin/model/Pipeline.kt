package model

import kotlinx.serialization.Serializable

@Serializable
data class Pipeline(
    val id: Long,
    val iid: Long,
    val project_id: Long,
    val status: String,
    val source: String,
    val ref: String,
    val sha: String,
    val name: String?,
    val web_url: String,
    val created_at: String, //Example: "2016-08-11T11:28:34.085Z"
    val updated_at: String, //Example: "2016-08-11T11:32:35.169Z"
    val finished_at: String? = null,
)